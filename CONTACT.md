## Contact Information

### Maintainers

 * Daniel Menelkir <dmenelkir@gmail.com>

### Bugs/issues/Whatever else

 * Please use the [issues page](https://github.com/menelkir/refind-menelkir/issues).
