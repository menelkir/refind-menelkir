refind-menelkir
===============

A custom theme based on refind-theme-minimal and refind-theme-regular

.. image:: https://gitlab.com/menelkir/refind-menelkir/raw/master/sshot.png

===========
Themes used
===========

:refind-theme-regular: https://github.com/munlik/refind-theme-regular
:refind-minimal:  https://github.com/EvanPurkhiser/rEFInd-minimal

============
Installation
============

1) Copy to your refind theems directory (usually in EFI/refind/themes/)

2) Add this line at the end of EFI/refind/refind.conf

*include themes/refind-menelkir/theme.conf*

=============
How to donate
=============

If you find this repo useful (don't forget to pay a visit to the related
repos too), you can buy me a beer:

:BTC: 3ECzX5UhcFSRv6gBBYLNBc7zGP9UA5Ppmn

:ETH: 0x7E17Ac09Fa7e6F80284a75577B5c1cbaAD566C1c

